#include "Motors.h"

boolean inMovement;

void move(directions direction) {
  switch (direction) {
    case FORWARD:
      while ((digitalRead(LS) == 0) && (digitalRead(RS) == 0)) {
        Motor.speed(MOTOR1, 65);
        Motor.speed(MOTOR2, 65);
        Serial.println("J'attends une intersection...");
        if ((digitalRead(LCS) == 1 && digitalRead(LS) == 1) || (digitalRead(RCS) == 1 && digitalRead(RS) == 1)) {
          break;
        } else if (digitalRead(LCS) == 1) {
          calibration(LEFT);
        } else if (digitalRead(RCS) == 1) {
          calibration(RIGHT);
        } else if (analogRead(P) > 500) {
          checkParkingSpace();
          break;
        }
      }
      inMovement = 1;
      break;
    case LEFT:
      // Turn Left

      Serial.println("1.C'EST BON !!!");
      // BEGIN : Intersection Placement
      delay(t);
      Motor.stop(MOTOR1);
      Motor.stop(MOTOR2);
      delay(t);
      // END : Intersection Placement
      while (inMovement == 1) {
        Serial.println("JE TOURNE A GAUCHE");
        Motor.speed(MOTOR1, -85);
        Motor.speed(MOTOR2, 85);
        if (digitalRead(LCS) == 1)
          inMovement = 0;
      }
      break;
    case RIGHT:
      // Turn Right

      Serial.println("2.C'EST BON !!!");
      // BEGIN : Intersection Placement
      delay(t);
      Motor.stop(MOTOR1);
      Motor.stop(MOTOR2);
      delay(t);
      // END : Intersection Placement
      while (inMovement == 1) {
        Serial.println("JE TOURNE A DROITE");
        Motor.speed(MOTOR1, 85);
        Motor.speed(MOTOR2, -85);
        if (digitalRead(RCS) == 1)
          inMovement = 0;
      }
      break;
    case DONT:
      Motor.stop(MOTOR1);
      Motor.stop(MOTOR2);
      delay(50);
      Motor.speed(MOTOR1, 100);
      Motor.speed(MOTOR2, -100);
      delay(1000);
      Motor.speed(MOTOR1, -100);
      Motor.speed(MOTOR2, 100);
      delay(1000);
      Motor.speed(MOTOR1, -100);
      Motor.speed(MOTOR2, -100);
      delay(250);
      Motor.speed(MOTOR1, 100);
      Motor.speed(MOTOR2, 100);
      delay(250);
      Motor.stop(MOTOR1);
      Motor.stop(MOTOR2);
      break;
  }
}
void calibration(directions direction) {
  switch (direction) {
    case LEFT:
      Motor.speed(MOTOR1, -60);
      Motor.speed(MOTOR2, 100);
      break;
    case RIGHT:
      Motor.speed(MOTOR1, 100);
      Motor.speed(MOTOR2, -60);
      break;
  }
}
