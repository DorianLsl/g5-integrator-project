#ifndef __CALCULS_H__
#define __CALCULS_H__

FastCRC16 CRC16;

boolean obstacle;
float initialValue,
      secondValue,
      calculationTime,
      speedCar,
      mps;
unsigned int distance,
         rpm,
         pulsesPerTurn = 20,
         radPsec,
         cmPsec;
unsigned long timeold;
volatile byte pulses;

int crc;

short parkingAvailable,
      posdix,
      posdiy,
      pospix,
      pospiy,
      Num1 = 0,
      Num2 = 0,
      Num3 = 0,
      Num4 = 0;

void checkParkingSpace();
float getSpeed();
void counter();

#endif
