#include "Calculs.h"

//300 étant une valeur choisie volontairement suite à des tests.
void checkParkingSpace() {
  Serial.println("Trouvé !");
  obstacle = true ;
  initialValue = millis();
  while (obstacle == true) {
    speedCar = getSpeed();
    Serial.println("Je calcule...");
    if (analogRead(P) < 500) {
      obstacle = false;
      secondValue = millis();
    }
  }
  calculationTime = (secondValue - initialValue) / 1000; // CONVERSION EN SECONDES
  Serial.print("Le calcul a duré : ");
  Serial.println(calculationTime);
  Serial.print("Avec une vitesse de : ");
  Serial.println(speedCar);
  distance = speedCar * calculationTime;
  Serial.println(distance);
  if (distance <= 5)
    parkingAvailable++;
}

float getSpeed() {
  // if (millis() - timeold >= 1000) {
  //Don't process interrupts during calculations
  detachInterrupt(0);
  rpm = (60 * 1000 / pulsesPerTurn ) / (millis() - timeold) * pulses;
  //mps = (rpm / 60) * 3, 25;
  //radPsec = ((2 * 3, 14) / 60) * rpm;;
  //mps = 3, 25 * radPsec;
  //cmPsec = mps * 100;
  timeold = millis();
  pulses = 0;
  //Serial.println(cmPsec, DEC);
  //Restart the interrupt processing
  attachInterrupt(0, counter, FALLING);
  //}
  return rpm;
}

void counter() {
  pulses++;
}

void sendData() {
  // Increment Number's Trame
  Num1 += 1;
  if (Num1 == 10) {
    Num1 = 0;
    Num2 += 1;
  }
  if (Num2 == 10) {
    Num2 = 0;
    Num3 += 1;
  }
  if (Num3 == 10) {
    Num3 = 0;
    Num4 += 1;
  }

  //CRC Calcul
  uint8_t  buf[14] = {6, 6, 0, 0, 5, Num4, Num3, Num2, Num1, x, y, parkingAvailable, pospix, pospiy};
  crc = CRC16.ccitt((int)buf, sizeof(buf)), DEC;

  //Send Trame
  uint8_t Trame[15] = {6, 6, 0, 0, 5, Num4, Num3, Num2, Num1, x + 5, y + 5, parkingAvailable + 5, pospix + 5, pospiy + 5, crc};
  vw_send((byte *) &Trame, sizeof(Trame)); // On envoie le message
  vw_wait_tx(); // On attend la fin de l'envoi
}

