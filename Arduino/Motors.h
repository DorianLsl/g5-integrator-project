#ifndef __MOTORS_H__
#define __MOTORS_H__

const int t = 250;
int x = 0;
int y = 0;
const int posVx = 5;
const int posVy = 1;
int rando;

enum directions {
  LEFT,
  RIGHT,
  FORWARD,
  DONT
};

void move(directions direction);
void calibration(directions direction);

#endif
