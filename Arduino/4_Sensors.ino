#include  <VirtualWire.h>
#include <FastCRC.h>

#include "Grove_I2C_Motor_Driver.h"
#include "Motors.h"
#include "Pathfinding.h"
#include "Calculs.h"

#define LS  5
#define LCS 3
#define RCS 4
#define RS  8
#define P   1  // Proximity Sensor
#define S   2
#define RF_TX_PIN 7 // Emitter Pin
/*
     LS LCS RCS RS
     -----------
   ||     ^     ||
    |     |     |
   ||     |     ||
     -----------
*/

void setup() {
  Serial.begin(9600);
  Motor.begin(0x0f);
  Serial.println("START MOTORS");
  vw_set_tx_pin(RF_TX_PIN); // Definir le pin de transmission

  // Initialisation de la bibliothèque VirtualWire
  vw_setup(2000);

  // Capteurs
  pinMode(LS, INPUT);
  pinMode(LCS, INPUT);
  pinMode(RS, INPUT);
  pinMode(RCS, INPUT);
  pinMode(P, INPUT);
  pinMode(S, INPUT);

  pulses = 0;
  rpm = 0;
  timeold = 0;
  parkingAvailable = 0;

  delay(1000);
  if ( posVx > x && (abs(posVx - x)  > abs(posVy - y) || posVy == y)) { // on compare la position voulue à notre position de base et on compare si le décalage est prioritaire à l'ascention.
    AB();//On appelle notre fonction
  } else if ( posVy > y && (abs(posVy - y)  > abs(posVx - x) || posVx == x) ) {
    AF();
  } else if ( posVx == x && posVy == y) { //On vérifie si la position correspond à la position voulue
    Serial.println("Vous êtes arrivé");
  } else { //Si la position voulu a pour ordonnée la même valeur que l'abscisse alors on choisi ce mouvement (pour le cas de D)
    AB();
  }
}//END SETUP

void loop() {
  sendData();
}
