#include "pathfinding.h"

void AB() { // Fonction type d'un vecteur
  move(FORWARD);
  //ON AVANCE TANT QUE ON CAPTE PAS UNE INTERSECTION
  // QUAND ON CAPTE L INTERSECTION ON ARRETE LES MOTEURS
  x = 1;  //On initialise La postion de la voiture sur l'axe x
  y = 0;  //On initialise La postion de la voiture sur l'axe y
  Serial.println("Je suis sur la position de B");
  if (posVx == 5 && posVy == 1 || posVx == 5 && posVy == 2 ) { //Si la position voulue correspond au point E ou G on oblige à faire un mouvement pour avoir le chemin le plus court
    //ON ORIENTE VERS BD ICI MOVE LEFT
    move(LEFT);
    BD();
  } else if ( posVx > x && (abs(posVx - x) > abs(posVy - y) || posVy == y)) { // on compare la position voulue à notre position de base et on compare si le décalage est prioritaire à l'ascention
    // ON CONTINUE TOUT DROIT
    BC();//On appelle la fonction correspondante
  } else if ( posVy > y && (abs(posVy - y ) > abs(posVx - x) || posVx == x) ) {
    // ON ORIENTE VERS BD ICI MOVE LEFT
    move(LEFT);
    BD();
  } else if ( posVx == x && posVy == y) {
    //SI LORSQUE ON DETECT L INTERSECTION CELLE CI CORRESPOND AU POINT ATTENDU ALORS MOTOR STOP
    move(DONT);
    Serial.println("Vous êtes arrivé");
  } else {
    rando = random(0, 2);
    switch (rando) {
      case 0 :
        // ON RESTE DROIT
        BC();
        break;
      case 1 :
        // ON ORIENTE VERS BD ICI MOVE LEFT
        move(LEFT);
        BD();
        break;
    }
  }
}

void AF() {
  move(FORWARD);

  x = 0;
  y = 2;
  Serial.println("Je suis sur la position de F");
  if (posVx == 3 && posVy == 4) {
    FI();
  } else if ( posVx > x && (abs(posVx - x) > abs(posVy - y) || posVy == y)) {
    move(RIGHT);
    FG();
  } else if ( posVy > y && (abs(posVy - y)  > abs(posVx - x) || posVx == x) ) {
    FI();
  } else if ( posVx == x && posVy == y) {
    move(DONT);
    Serial.println("Vous êtes arrivé");
  } else {
    rando = random(0, 2);
    switch (rando) {
      case 0 :
        move(RIGHT);
        FG();
        break;
      case 1 :
        FI();
        break;
    }
  }
}

void BC() {

  if (inMovement == 1) {
    Motor.speed(MOTOR1, 65);
    Motor.speed(MOTOR2, 65);
    delay(t);
    move(FORWARD);
    inMovement = 0;
  } else
    move(FORWARD);

  x = 6;
  y = 0;

  Serial.println("Je suis sur la position de C");
  if ( posVx == x && posVy == y) {
    move(DONT);
    Serial.println("Vous êtes arrivé");
  } else {
    move(LEFT);
    CH();
  }
}

void BD() {
  //ON AVANCE TANT QUE Y A PAS D INTERSECTION
  // QUAND Y EN A UNE ON STOP
  move(FORWARD);

  x = 1;
  y = 1;
  Serial.println("Je suis sur la position de D");
  if ( posVx == x && posVy == y) {
    //SI ON EST SUR LA POSITION ON STOP
    move(DONT);
    Serial.println("Vous êtes arrivé");
  } else {
    // ON S ORIENTE VERS DE ICI MOVE RIGHT
    move(RIGHT);
    DE();
  }
}

void BA() {
  if (inMovement == 1) {
    Motor.speed(MOTOR1, 100);
    Motor.speed(MOTOR2, 100);
    delay(t);
    move(FORWARD);
    inMovement = 0;
  } else
    move(FORWARD);

  x = 0;
  y = 0;

  Serial.println("Je suis sur la position A");
  if ( posVx == x && posVy == y) {
    move(DONT);
    Serial.println("Vous êtes arrivé");
  } else {
    move(RIGHT);
    AF();
  }
}

void CB() {
  move(FORWARD);
  x = 1;
  y = 0;

  Serial.println("Je suis sur la position B");
  if ( posVx < x && (abs(posVx - x) > abs(posVy - y ) || posVy == y)) {
    BA();
  } else if ( posVy > y && (abs(posVy - y)  > abs(posVx - x) || posVx == x)) {
    move(RIGHT);
    BD();
  } else if ( posVx == x && posVy == y) {
    move(DONT);
    Serial.println("Vous êtes arrivé");
  } else {
    rando = random(0, 2);
    switch (rando) {
      case 0 :
        BA();
        break;
      case 1 :
        move(RIGHT);
        BD();
        break;
    }
  }
}

void CH() {
  move(FORWARD);

  x = 6;
  y = 2;

  Serial.println("Je suis sur la position H");
  if ( posVx < x && (abs(posVx - x ) > abs(posVy - y) || posVy == y)) {
    move(LEFT);
    HG();
  } else if ( posVy > y && (abs(posVy - y)  > abs(posVx - x) || posVx == x)) {
    HP();
  } else if ( posVx == x && posVy == y) {
    move(DONT);
    Serial.println("Vous êtes arrivé");
  } else {
    move(LEFT);
    HG();
  }
}

void DB() {
  move(FORWARD);
  x = 1;
  y = 0;

  Serial.println("Je suis sur la position B");
  if ( posVx < x) {
    move(RIGHT);
    BA();
  } else if ( posVx > x) {
    move(LEFT);
    BC();
  } else if ( posVx == x && posVy == y) {
    move(DONT);
    Serial.println("Vous êtes arrivé");
  }
}

void DE() {
  move(FORWARD);
  //ON AVANCE TANT QUE Y A PAS D INTERSECTION
  //QUAND Y EN A UNE ON STOP
  x = 5;
  y = 1;
  Serial.println("Je suis sur la position de E");
  if ( posVx == x && posVy == y) {
    // SI ON EST SUR LA POSITION ON STOP
    move(DONT);
    Serial.println("Vous êtes arrivé");
  } else {
    //ON S ORIENTE VERS EG ICI MOVE LEFT
    move(LEFT);
    EG();
  }
}

void ED() {

  move(FORWARD);
  x = 1;
  y = 1;

  Serial.println("Je suis sur la position D");
  if ( posVx == x && posVy == y) {
    move(DONT);
    Serial.println("Vous êtes arrivé");
  } else {
    move(LEFT);
    DB();
  }
}

void EG() {
  move(FORWARD);
  //ON AVANCE TANT QUE Y A PAS D INTERSECTION
  // QUAND Y EN A UNE ON STOP

  x = 5;
  y = 2;

  Serial.println("Je suis sur la position G");

  if ( posVx < x && (abs(posVx - x) > abs(posVy - y ) || posVy == y)) {
    //ON S ORIENTE VERS GF ICI MOVE LEFT
    move(LEFT);
    GF();
  } else if ( posVy > y && (abs(posVy - y ) > abs(posVx - x) || posVx == x)) {
    //ON S ORIENTE VERS GO ICI ON VA TOUT DROIT
    GO();
  } else if ( posVx > x && (abs(posVx - x) > abs(posVy - y) || posVy == y)) {
    //ON S ORIENTE VERS GH DONC ICI MOVE RIGHT
    move(RIGHT);
    GH();
  } else if ( posVx == x && posVy == y) {
    //SI ON EST SUR LA POSITION ON STOP
    move(DONT);
    Serial.println("Vous êtes arrivé");
  } else {
    rando = random(0, 3);
    switch (rando) {
      case 0 :
        //ON SORIENTE VERS GF DONC ICI MOVE LEFT
        move(LEFT);
        GF();
        break;
      case 1 :
        // ON S ORIENTE VERS GO DONC ICI TOUT DROIT
        GO();
        break;
      case 2 :
        //ON S ORIENTE VERS GH DONC ICI MOVE RIGHT
        move(RIGHT);
        GH();
        break;
    }
  }
}

void FA() {
  if (inMovement == 1) {
    Motor.speed(MOTOR1, 100);
    Motor.speed(MOTOR2, 100);
    delay(t);
    move(FORWARD);
    inMovement = 0;
  } else
    move(FORWARD);

  x = 0;
  y = 0;

  Serial.println("Je suis sur la position A");
  if ( posVx == x && posVy == y) {
    move(DONT);
    Serial.println("Vous êtes arrivé");
    AB();
  }
}

void FI() {
  if (inMovement == 1) {
    Motor.speed(MOTOR1, 65);
    Motor.speed(MOTOR2, 65);
    delay(t);
    move(FORWARD);
    inMovement = 0;
  } else
    move(FORWARD);

  x = 0;
  y = 4;

  Serial.println("Je suis sur la position I");
  if ( posVx > x && (abs(posVx - x) > abs(posVy - y) || posVy == y)) {
    move(RIGHT);
    IJ();
  } else if ( posVy > y && (abs(posVy - y)  > abs(posVx - x) || posVx == x)) {
    IL();
  } else if ( posVx == x && posVy == y) {
    move(DONT);
    Serial.println("Vous êtes arrivé");
  } else {
    rando = random(0, 2);
    switch (rando) {
      case 0 :
        move(RIGHT);
        IJ();
        break;
      case 1 :
        IL();
        break;
    }
  }
}

void FG() {
  if (inMovement == 1) {
    Motor.speed(MOTOR1, 100);
    Motor.speed(MOTOR2, 100);
    delay(t);
    move(FORWARD);
    inMovement = 0;
  } else
    move(FORWARD);

  x = 5;
  y = 2;

  Serial.println("Je suis sur la position G");
  if ( posVx > x && (abs(posVx - x) > abs(posVy - y) || posVy == y)) {
    GH();
  } else if ( posVy > y && (abs(posVy - y ) > abs(posVx - x) || posVx == x)) {
    move(LEFT);
    GO();
  } else if ( posVy < y && (abs(posVy - y)  > abs(posVx - x ) || posVx == x)) {
    move(RIGHT);
    GE();
  } else if ( posVx == x && posVy == y) {
    move(DONT);
    Serial.println("Vous êtes arrivé");
  }
}

void GE() {
  if (inMovement == 1) {
    Motor.speed(MOTOR1, 100);
    Motor.speed(MOTOR2, 100);
    delay(t);
    move(FORWARD);
    inMovement = 0;
  } else
    move(FORWARD);

  x = 5;
  y = 1;

  Serial.println("Je suis sur la position E");
  if ( posVx == x && posVy == y) {
    move(DONT);
    Serial.println("Vous êtes arrivé");
  } else {
    move(RIGHT);
    ED();
  }
}

void GF() {
  if (inMovement == 1) {
    Motor.speed(MOTOR1, 100);
    Motor.speed(MOTOR2, 100);
    delay(t);
    move(FORWARD);
    inMovement = 0;
  } else
    move(FORWARD);

  x = 0;
  y = 2;

  Serial.println("Je suis sur la position F");
  if ( posVy > y ) {
    move(RIGHT);
    FI();
  } else if ( posVy < y) {
    move(LEFT);
    FA();
  } else if ( posVx == x && posVy == y) {
    move(DONT);
    Serial.println("Vous êtes arrivé");
  }
}

void GH() {
  if (inMovement == 1) {
    Motor.speed(MOTOR1, 100);
    Motor.speed(MOTOR2, 100);
    delay(t);
    move(FORWARD);
    inMovement = 0;
  } else
    move(FORWARD);
  x = 6;
  y = 2;

  Serial.println("Je suis sur la position H");
  if ( posVy > y ) {
    move(LEFT);
    HP();
  } else if ( posVy < y) {
    move(RIGHT);
    HC();
  } else if ( posVx == x && posVy == y) {
    Serial.println("Vous êtes arrivé");
  }
}

void GO() {
  if (inMovement == 1) {
    Motor.speed(MOTOR1, 100);
    Motor.speed(MOTOR2, 100);
    delay(t);
    move(FORWARD);
    inMovement = 0;
  } else
    move(FORWARD);
  //ON AVANCE TANT QUE Y A PAS D INTERSECTION
  //ON STOP QUAND Y EN A UNE

  x = 5;
  y = 5;

  Serial.println("Je suis sur la position O");
  rando = random(0, 2);
  if ( posVx > x ) {
    //ON S ORIENTE VERS OP ICI MOVE RIGHT
    move(RIGHT);
    OP();
  } else if ( posVx < x ) {
    //ON S ORIENTE VERS ON DONC ICI MOVE LEFT
    move(LEFT);
    ON();
  } else if ( posVx == x && posVy == y) {
    // ON VERIFIE NOTRE POSITION ET ON STOP SI C EST BON
    move(DONT);
    Serial.println("Vous êtes arrivé");
  }
}

void HC() {
  if (inMovement == 1) {
    Motor.speed(MOTOR1, 100);
    Motor.speed(MOTOR2, 100);
    delay(t);
    move(FORWARD);
    inMovement = 0;
  } else
    move(FORWARD);

  x = 6;
  y = 0;

  Serial.println("Je suis sur la position C");
  if ( posVx == x && posVy == y) {
    move(DONT);
    Serial.println("Vous êtes arrivé");
  } else {
    move(RIGHT);
    CB();
  }
}

void HG() {
  move(FORWARD);

  x = 5;
  y = 2;

  Serial.println("Je suis sur la position G");
  if ( posVx < x && (abs(posVx - x ) > abs(posVy - y) || posVy == y)) {
    GF();
  } else if ( posVy > y && (abs(posVy - y ) > abs(posVx - x) || posVx == x)) {
    move(RIGHT);
    GO();
  } else if ( posVy < y && (abs(posVy - y  ) > abs(posVx - x) || posVx == x)) {
    move(LEFT);
    GE();
  } else if ( posVx == x && posVy == y) {
    move(DONT);
    Serial.println("Vous êtes arrivé");
  }
}

void HP() {
  if (inMovement == 1) {
    Motor.speed(MOTOR1, 100);
    Motor.speed(MOTOR2, 100);
    delay(t);
    move(FORWARD);
    inMovement = 0;
  } else
    move(FORWARD);
  x = 6;
  y = 5;

  Serial.println("Je suis sur la position P");
  if ( posVx == x && posVy == y) {
    move(DONT);
    Serial.println("Vous êtes arrivé");
  } else {
    move(LEFT);
    PO();
  }
}

void IF() {
  if (inMovement == 1) {
    Motor.speed(MOTOR1, 100);
    Motor.speed(MOTOR2, 100);
    delay(t);
    move(FORWARD);
    inMovement = 0;
  } else
    move(FORWARD);

  x = 0;
  y = 2;

  Serial.println("Je suis sur la position F");
  if ( posVx > x && (abs(posVx - x) > abs( posVy - y) || posVy == y)) {
    move(LEFT);
    FG();
  } else if ( posVy < y && (abs(posVy - y)  > abs( posVx - x ) || posVx == x)) {
    FA();
  } else if ( posVx == x && posVy == y) {
    move(DONT);
    Serial.println("Vous êtes arrivé");
  }
}

void IJ() {
  move(FORWARD);

  x = 1;
  y = 4;

  Serial.println("Je suis sur la position J");
  if ( posVx > x && (abs(posVx - x) > abs(posVy - y) || posVy == y)) {
    JK();
  } else if ( posVy > y && (abs(posVy - y)  > abs(posVx - x) || posVx == x)) {
    move(LEFT);
    JM();
  } else if ( posVx == x && posVy == y) {
    move(DONT);
    Serial.println("Vous êtes arrivé");
  }
}

void IL() {
  if (inMovement == 1) {
    Motor.speed(MOTOR1, 65);
    Motor.speed(MOTOR2, 65);
    delay(t);
    move(FORWARD);
    inMovement = 0;
  } else
    move(FORWARD);

  x = 0;
  y = 5;

  Serial.println("Je suis sur la position L");
  if ( posVx == x && posVy == y) {
    move(DONT);
    Serial.println("Vous êtes arrivé");
  } else {
    move(RIGHT);
    LM();
  }
}

void JI() {
  if (inMovement == 1) {
    Motor.speed(MOTOR1, 100);
    Motor.speed(MOTOR2, 100);
    delay(t);
    move(FORWARD);
    inMovement = 0;
  } else
    move(FORWARD);

  x = 0;
  y = 4;

  Serial.println("Je suis sur la position I");
  rando = random(0, 2);
  if ( posVy < y) {
    move(LEFT);
    IF();
  } else if ( posVy > y) {
    move(RIGHT);
    IL();
  } else if ( posVx == x && posVy == y) {
    move(DONT);
    Serial.println("Vous êtes arrivé");
  }
}

void JK() {
  if (inMovement == 1) {
    Motor.speed(MOTOR1, 100);
    Motor.speed(MOTOR2, 100);
    delay(t);
    move(FORWARD);
    inMovement = 0;
  } else
    move(FORWARD);
  x = 3;
  y = 4;

  Serial.println("Je suis sur la position K");
  if ( posVx == x && posVy == y) {
    move(DONT);
    Serial.println("Vous êtes arrivé");
  } else {
    move(LEFT);
    KN();
  }
}

void JM() {
  move(FORWARD);

  x = 1;
  y = 5;

  Serial.println("Je suis sur la position M");
  rando = random(0, 2);
  if ( posVx > x ) {
    move(RIGHT);
    MN();
  } else if ( posVx < x ) {
    move(LEFT);
    ML();
  } else if ( posVx == x && posVy == y) {
    move(DONT);
    Serial.println("Vous êtes arrivé");
  }
}

void KJ() {
  move(FORWARD);

  x = 1;
  y = 4;

  Serial.println("Je suis sur la position J");
  if ( posVx < x && (abs(posVx - x) > abs(posVy - y) || posVy == y) ) {
    JI();
  } else if ( posVy > y && (abs(posVy - y)  > abs(posVx - x) || posVx == x)) {
    move(RIGHT);
    JM();
  } else if ( posVx == x && posVy == y) {
    move(DONT);
    Serial.println("Vous êtes arrivé");
  }
}

void KN() {
  move(FORWARD);

  x = 3;
  y = 5;

  Serial.println("Je suis sur la position N");
  rando = random(0, 2);
  if ( posVx < x ) {
    move(LEFT);
    NM();
  } else if ( posVy > y ) {
    move(RIGHT);
    NO();
  } else if ( posVx == x && posVy == y) {
    move(DONT);
    Serial.println("Vous êtes arrivé");
  }
}

void LI() {
  move(FORWARD);

  x = 0;
  y = 4;

  Serial.println("Je suis sur la position I");
  if ( posVx > x && (abs(posVx - x) > abs(posVy - y) || posVy == y) ) {
    move(LEFT);
    IJ();
  } else if ( posVy < y && (abs(posVy - y)  > abs(posVx - x) || posVx == x)) {
    IF();
  } else if ( posVx == x && posVy == y) {
    move(DONT);
    Serial.println("Vous êtes arrivé");
  }
}

void LM() {
  move(FORWARD);

  x = 1;
  y = 5;

  Serial.println("Je suis la position M");
  if ( posVx > x && (abs(posVx - x) > abs(posVy - y) || posVy == y) ) {
    MN();
  } else if ( posVy < y && (abs(posVy - y)  > abs( posVx - x) || posVx == x)) {
    move(RIGHT);
    MJ();
  } else if ( posVx == x && posVy == y) {
    move(DONT);
    Serial.println("Vous êtes arrivé");
  }
}

void MN() {
  if (inMovement == 1) {
    Motor.speed(MOTOR1, 100);
    Motor.speed(MOTOR2, 100);
    delay(t);
    move(FORWARD);
    inMovement = 0;
  } else
    move(FORWARD);

  x = 3;
  y = 5;

  Serial.println("Je suis sur la position N");
  if ( posVx > x && (abs(posVx - x) > abs(posVy - y) || posVy == y) ) {
    NO();
  } else if ( posVy < y && (abs(posVy - y)  > abs(posVx - x) || posVx == x)) {
    move(RIGHT);
    NK();
  } else if ( posVx == x && posVy == y) {
    move(DONT);
    Serial.println("Vous êtes arrivé");
  }
}

void ML() {
  move(FORWARD);

  x = 0;
  y = 5;

  Serial.println("Je suis sur la position L");
  if ( posVx == x && posVy == y) {
    move(DONT);
    Serial.println("Vous êtes arrivé");
  } else {
    move(LEFT);
    LI();
  }
}

void MJ() {
  move(FORWARD);

  x = 1;
  y = 4;

  Serial.println("Je suis sur la position J");
  if ( posVx < x ) {
    move(RIGHT);
    JI();
  } else if ( posVy > y ) {
    move(LEFT);
    JK();
  } else if ( posVx == x && posVy == y) {
    move(DONT);
    Serial.println("Vous êtes arrivé");
  }
}

void NM() {
  if (inMovement == 1) {
    Motor.speed(MOTOR1, 100);
    Motor.speed(MOTOR2, 100);
    delay(t);
    move(FORWARD);
    inMovement = 0;
  } else
    move(FORWARD);

  x = 1;
  y = 5;

  Serial.println("Je suis sur la position M");
  if ( posVx < x && (abs(posVx - x ) > abs(posVy - y) || posVy == y) ) {
    ML();
  } else if ( posVy < y && (abs(posVy - y)  > abs(posVx - x ) || posVx == x)) {
    move(LEFT);
    MJ();
  } else if ( posVx == x && posVy == y) {
    move(DONT);
    Serial.println("Vous êtes arrivé");
  }
}

void NK() {
  move(FORWARD);

  x = 3;
  y = 4;

  Serial.println("Je suis sur la position K");
  if ( posVx == x && posVy == y) {
    move(DONT);
    Serial.println("Vous êtes arrivé");
  } else {
    move(RIGHT);
    KJ();
  }
}

void NO() {
  if (inMovement == 1) {
    Motor.speed(MOTOR1, 100);
    Motor.speed(MOTOR2, 100);
    delay(t);
    move(FORWARD);
    inMovement = 0;
  } else
    move(FORWARD);
  x = 5;
  y = 5;

  Serial.println("Je suis sur la position O");
  if ( posVx > x && (abs(posVx - x ) > abs(posVy - y) || posVy == y) ) {
    OP();
  } else if ( posVy < y && (abs(posVy - y)  > abs(posVx - x ) || posVx == x)) {
    move(RIGHT);
    OG();
  } else if ( posVx == x && posVy == y) {
    move(DONT);
    Serial.println("Vous êtes arrivé");
  }
}

void ON() {
  if (inMovement == 1) {
    Motor.speed(MOTOR1, 100);
    Motor.speed(MOTOR2, 100);
    delay(t);
    move(FORWARD);
    inMovement = 0;
  } else
    move(FORWARD);

  x = 3;
  y = 5;

  Serial.println("Je suis sur la position N");
  if ( posVx < x && (abs(posVx - x) > abs(posVy - y) || posVy == y) ) {
    NM();
  } else if ( posVy < y && (abs(posVy - y)  > abs(posVx - x) || posVx == x)) {
    move(LEFT);
    NK();
  } else if ( posVx == x && posVy == y) {
    move(DONT);
    Serial.println("Vous êtes arrivé");
  }
}

void OP() {
  if (inMovement == 1) {
    Motor.speed(MOTOR1, 100);
    Motor.speed(MOTOR2, 100);
    delay(t);
    move(FORWARD);
    inMovement = 0;
  } else
    move(FORWARD);
  x = 6;
  y = 5;

  Serial.println("Je suis sur la position P");
  if ( posVx == x && posVy == y) {
    move(DONT);
    Serial.println("Vous êtes arrivé");
  } else {
    move(RIGHT);
    PH();
  }
}

void OG() {
  move(FORWARD);

  x = 5;
  y = 2;

  Serial.println("Je suis sur la position G");
  if ( posVx < x  ) {
    move(RIGHT);
    GF();
  } else if ( posVy > y) {
    move(LEFT);
    GH();
  } else if ( posVx == x && posVy == y) {
    move(DONT);
    Serial.println("Vous êtes arrivé");
  }
}

void PO() {
  move(FORWARD);

  x = 5;
  y = 5;

  Serial.println("Je suis sur la position O");
  if ( posVx < x && (abs(posVx - x) > abs(posVy - y) || posVy == y) ) {
    ON();
  } else if ( posVy < y && (abs(posVy - y)  > abs(posVx - x) || posVx == x)) {
    move(LEFT);
    OG();
  } else if ( posVx == x && posVy == y) {
    move(DONT);
    Serial.println("Vous êtes arrivé");
  }
}

void PH() {
  move(FORWARD);

  x = 6;
  y = 2;

  Serial.println("Je suis sur la position H");
  if ( posVx < x && (abs(posVx - x) > abs(posVy - y) || posVy == y) ) {
    move(RIGHT);
    HG();
  } else if ( posVy < y && (abs(posVy - y)  > abs(posVx - x) || posVx == x)) {
    HC();
  } else if ( posVx == x && posVy == y) {
    move(DONT);
    Serial.println("Vous êtes arrivé");
  }
}
