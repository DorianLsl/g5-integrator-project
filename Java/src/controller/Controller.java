package controller;

import view.ViewTrame;

public class Controller {
	ViewTrame myView;

	public Controller() {
		myView = new ViewTrame(this);
		myView.setVisible(true);
	}
}
