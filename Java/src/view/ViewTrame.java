package view;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.Controller;
import util.ArduinoSerial;

import javax.swing.JLabel;
import java.awt.BorderLayout;
import javax.swing.SwingConstants;
import java.awt.Font;

public class ViewTrame extends JFrame {

	private JPanel contentPane;
	private Controller controller;
	private ArduinoSerial myArduino;
	private JLabel lblNewLabel; 

	/**
	 * Create the frame.
	 */
	public ViewTrame(Controller myController) {
		this.controller = myController;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(560, 325);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		lblNewLabel = new JLabel();
		lblNewLabel.setFont(new Font("Century Gothic", Font.BOLD, 18));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(lblNewLabel, BorderLayout.CENTER);
		
		myArduino = new ArduinoSerial("COM5");
		Thread t = new Thread() {
			@Override
			public void run() {
				myArduino.initialize();
				while(true) {
					lblNewLabel.setText(myArduino.read());
				}
			}
		};
		t.start();
	}
}
